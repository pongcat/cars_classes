{

;============================================================
;============================================================
; Mudman
;============================================================
;============================================================
Name       	"Mudman"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars/mudman/body.prm"
MODEL 	1 	"cars/mudman/wheel-flat-l.prm"
MODEL 	2 	"cars/mudman/wheel-flat-r.prm"
MODEL 	3 	"cars/mudman/axle.prm"
MODEL 	4 	"cars/mudman/spring.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 	"cars/mudman/hull.hul"
TPAGE 	"cars/mudman/car.bmp"
;)TCARBOX   "cars/mudman/carbox.bmp"
;)TSHADOW 	"cars/mudman/shadow.bmp"
;)SHADOWTABLE 	-58 58 81.5 -83.5 9.0
EnvRGB 	150 150 150


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
;)Statistics    TRUE
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	5 			; Skill level (rookie, amateur, ...)
TopEnd     	4023.268066 			; Actual top speed (mph) for frontend bars
Acc        	4.817026 			; Acceleration rating (empirical)
Weight     	2.300000 			; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 			; Max Revs (for rev counter, deprecated...)


;====================
; Handling related stuff
;====================

SteerRate  	2.600000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	43.000000 			; Car's theoretical top speed (not including friction...)
DownForceMod	3.000000 			; Down force modifier when car on floor
CoM        	0.000000 -32.000000 0.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon generation offset
;)Flippable	false 			; Rotor car effect
;)Flying   	false 			; Flying like the UFO car
;)ClothFx  	false 			; Mystery car cloth effect


;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 12.000000 0.000000
Mass       	2.300000
Inertia    	2700.000000 0.000000 0.000000
           	0.000000 2500.000000 0.000000
           	0.000000 0.000000 1300.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air resistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	25.000000 			; AngRes scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body


;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-32.000000 28.000000 34.000000
Offset2  	-0.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.350000
EngineRatio 	44000.000000
Radius      	15.250000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	15.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.045000
Grip            	0.025000
StaticFriction  	1.900000
KineticFriction 	1.900000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	32.000000 28.000000 34.000000
Offset2  	0.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.350000
EngineRatio 	44000.000000
Radius      	15.250000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	15.100000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.045000
Grip            	0.025000
StaticFriction  	1.900000
KineticFriction 	1.900000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-32.000000 28.000000 -36.600000
Offset2  	-0.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	45000.000000
Radius      	15.250000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	15.100000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.032000
Grip            	0.030000
StaticFriction  	1.900000
KineticFriction 	1.900000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	32.000000 28.000000 -36.600000
Offset2  	0.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	45000.000000
Radius      	15.250000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	30.000000
SkidWidth   	15.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.032000
Grip            	0.030000
StaticFriction  	1.900000
KineticFriction 	1.900000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	4
Offset      	-18.000000 4.000000 34.000000
Length      	7.000000
Stiffness   	275.000000
Damping     	8.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	4
Offset      	18.000000 4.000000 34.000000
Length      	7.000000
Stiffness   	275.000000
Damping     	8.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	4
Offset      	-18.000000 4.000000 -36.600000
Length      	7.000000
Stiffness   	300.000000
Damping     	8.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	4
Offset      	18.000000 4.000000 -36.600000
Length      	7.000000
Stiffness   	300.000000
Damping     	8.000000
Restitution 	-0.900000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	3
Offset      	-11.200000 20.000000 34.000000
Length      	17.600000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	3
Offset      	11.200000 20.000000 34.000000
Length      	17.600000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	3
Offset      	-11.200000 20.000000 -36.600000
Length      	17.600000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	3
Offset      	11.200000 20.000000 -36.600000
Length      	17.600000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	21.000000 -5.000000 38.000000
Direction   	0.000000 -1.000000 0.000000
Length      	20.000000
Stiffness   	1000.000000
Damping     	3.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	93.820000
UnderRange  	3835.356934
UnderFront  	197.100006
UnderRear   	556.727966
UnderMax    	0.510582
OverThresh  	462.209991
OverRange   	1182.744873
OverMax     	1.000000
OverAccThresh  	37.340000
OverAccRange   	625.908142
PickupBias     	13106
BlockBias      	22936
OvertakeBias   	19660
Suspension     	16383
Aggression     	0
}           	; End AI

}

AAAAAAAA
