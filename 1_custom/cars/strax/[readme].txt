Car information
================================================================
Car name: Strax
Car Type: Repaint
Top speed: 50 kph
Rating/Class: 2 (Amateur)
Installed folder: ...\cars\strax
Description: 

This is Strax, a repaint of Hi-Ban's Fast Traxx. Strax use
the original body and parameters of Fast Traxx. I only renamed
the car, did a new skin and a new carbox, to avoid copyright
issues with the real Tyco brand "Fast Traxx".

Have fun!
-Kiwi

Author Information
================================================================
Texture and Carbox: Kiwi

 
Construction
================================================================
Base model: Fast Traxx (by Hi-Ban)
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention me and the original author in the credits.

Version 1.1 from February 12th, 2020
Version 1.0 from April 4th, 2019