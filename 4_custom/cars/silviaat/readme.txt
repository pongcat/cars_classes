Car information
================================================================
Car name : S13 Alltune
Car Type : Repaint/Reparam (Mladen's Nissan Silvia S13)
Top speed : 41 mph/66 kph
Rating/Class : 4 (pro)
Installed folder : ...\cars\silviaat
Description : A modification of the Silvia S13 found at http://revoltzone.net/cars/11701/Nissan%20S13%20Pack%20%28Pack%29
Parameters changed and model scaled to better match RV's stocks.
Texture re-done from scratch (also to match stocks), loosely based on inspiration from Speed Dreams. Re: "why 'alltune?'" : it was either that or 'tsubashi'
Also remapped a few things and tweaked vertex shading here and there to make things look cleaner, or at least match the look I was going for.

Rear-wheel drive, but has just enough power going to the front wheels that it isn't *guaranteed* to spin out of control when a battery is used.
Acceleration is pretty much midline, with top speed slightly above average.
Relatively stable, but the body is round enough that it likes to tumble around when hit.

Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com

Construction
================================================================
Base : Nissan Silvia S13
Editor(s) used : ChaosTools/RVMinis for basic tweaking, blender for vertex shading and further tweaking, gimp for skin/carbox, inkscape for rims, mousepad for params, probably a couple other things I don't remember because this car's been in the works for months

Changelog
================================================================
1.0 / 07 May 2014
 - Initial Release

1.1 / 30 May 2014
 - Widened body (1.1x)
 - Spent hours messing about with shading just to end up saying screw it it's good enough already   i.imgur.com/5VAdoEZ.png
 - I mean seriously blender what is this supposed to be   i.imgur.com/eVJ3Qzn.png
 - Additional enhanced skin (thanks MightyCucumber)   i.imgur.com/1hKCbR3.png

1.1.1 / ''
 - Shadow fixed for widened body

Additional Credits
================================================================
Mladen007 for original car
Jigebren for blender plugin
Creators of ChaosTools and RVMinis for various tools
TheMeAndMe for racekit spoiler
I don't remember which param notes I referenced so just credit to all of them yay
MightyCucumber for shiny things

Copyright / Permissions
================================================================
You may do whatever you want with this car, providing the terms of the original author are met (see readme-original.txt).
Image sources (XCF,SVG), .blend, and other such stuff in RVL thread download.