{

;============================================================
;============================================================
; Quag Hopper
;============================================================
;============================================================
Name      	"Quag Hopper"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\frogger\body.prm"
MODEL 	1 	"cars\frogger\wheelfl.prm"
MODEL 	2 	"cars\frogger\wheelfr.prm"
MODEL 	3 	"cars\frogger\wheelbl.prm"
MODEL 	4 	"cars\frogger\wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"cars\frogger\spring.prm"
MODEL 	9 	"cars\frogger\axle.prm"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 		"cars\frogger\car.bmp"
TCARBOX		"cars\frogger\carbox.bmp"
TSHADOW		"cars\frogger\shadow.bmp"
SHADOWTABLE	-56.5 55.0 58.8 -58.4 -2.5
COLL 		"cars\frogger\hull.hul"
EnvRGB 	100 100 100

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
Statistics	TRUE
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	0 			; Skill level (rookie, amateur, ...)
TopEnd     	2975.636963 			; Actual top speed (mph) for frontend bars
Acc        	8.855534 			; Acceleration rating (empirical)
Weight     	1.000000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	2 				; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	36.500000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 1.600000 5.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.200000
Inertia    	1000.000000 0.000000 0.000000
           	0.000000 1700.000000 0.000000
           	0.000000 0.000000 560.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	30.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-21.200000 3.000000 30.000000
Offset2  	-4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	5900.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	10.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.800000
KineticFriction 	1.650000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	21.200000 3.000000 30.000000
Offset2  	4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	5900.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	10.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.800000
KineticFriction 	1.650000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-18.300000 3.000000 -32.000000
Offset2  	-4.500000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	6000.000000
Radius      	11.300000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	12.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.015000
StaticFriction  	1.780000
KineticFriction 	1.620000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	18.300000 3.000000 -32.000000
Offset2  	4.500000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	6000.000000
Radius      	11.300000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	12.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.015000
StaticFriction  	1.780000
KineticFriction 	1.620000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	8
Offset      	-15.500000 -9.500000 30.000000
Length      	14.000000
Stiffness   	290.000000
Damping     	6.000000
Restitution 	-1.000000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	8
Offset      	15.500000 -9.500000 30.000000
Length      	14.000000
Stiffness   	290.000000
Damping     	6.000000
Restitution 	-1.000000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	8
Offset      	-13.000000 -14.000000 -31.500000
Length      	13.000000
Stiffness   	300.000000
Damping     	6.000000
Restitution 	-1.000000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	8
Offset      	13.000000 -14.000000 -31.500000
Length      	13.000000
Stiffness   	300.000000
Damping     	6.000000
Restitution 	-1.000000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	9
Offset      	-3.700000 -2.290000 30.000000
Length      	17.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	9
Offset      	3.700000 -2.290000 30.000000
Length      	17.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	-3.700000 -2.290000 -30.000000
Length      	17.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	9
Offset      	3.700000 -2.290000 -30.000000
Length      	17.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	14.900000 -13.500000 -20.000000
Direction   	0.000000 -1.000000 0.000000
Length      	17.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	150.000000
UnderRange  	3098.658691
UnderFront	 	2248.433105
UnderRear   	1541.193848
UnderMax    	0.320000
OverThresh  	1713.217529
OverRange   	1831.650024
OverMax     	0.900000
OverAccThresh  	89.169998
OverAccRange   	400.000000
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

C6E1F417