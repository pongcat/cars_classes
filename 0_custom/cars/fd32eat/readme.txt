﻿Car information
================================================================
Car name                : Eatium
Car Type  		: Remodel
Top speed 		: 34.7 mph
Rating/Class   		: Amateur
Installed folder       	: ...\cars\fd32eat

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Van (by BurnRubr)
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul
Credits to Insane Racing by Codemasters for car mesh
Credits to BurnRubr for the conversion of the car to Re-Volt

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.