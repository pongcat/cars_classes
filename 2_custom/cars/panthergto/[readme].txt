Car information
================================================================
Car name: Panther
Car Type: Conversion
Top speed: 63 kph
Rating/Class: 3 (Semi-Pro)
Installed folder: ...\cars\panthergto
Description: 

Panther is a mixture of the 1980's cars DeTomaso Pantera and Ferrari 288 GTO, but it does not share
the rear-wheel drive from the real-life counterparts. Instead it's a FWD-car, with lack in acceleration,
but very high top speed.

Included is an alternate texture without decals and different rims.

The model is based on a free 3d model from TurboSquid, made by perrymeichon.

Have fun with Panther!

Kiwi (Conversion, Remodel, Textures), Xarc (Remodel, Parameters)


Author Information
================================================================
Modeling work: Kiwi & Xarc
Textures: Kiwi
Parameters: Xarc
 
Construction
================================================================
Base: Car02 by perrymeichon (www.turbosquid.com)
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Additional Credits 
================================================================
+ Thanks to Kipy for his help with the hul-file.

Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention us and the original author in the credits.

HAVE FUN!

Kiwi & Xarc

Version 2.0 from March 16th, 2019